# distribution based on: debian 9.8
Bootstrap:docker
From:debian:9.8-slim

# container for blast v2.9.0
# Build:
# sudo singularity build blast_v2.9.0.sif Singularity.blast_v2.9.0

%environment
export LC_ALL=C
export LC_NUMERIC=en_GB.UTF-8
export PATH="/opt/miniconda/bin:$PATH"

#%labels
#COPYRIGHT INRAe GAFL 2020
#AUTHOR I2B team
#VERSION 1.1
#LICENSE MIT
#DATE_MODIF MYDATEMODIF
#blast (v2.9.0)

%help
Container for blast
BLAST+ is a new suite of BLAST tools that utilizes the NCBI C++ Toolkit.
http://blast.ncbi.nlm.nih.gov/Blast.cgi?PAGE_TYPE=BlastDocs

Version: 2.9.0
Package installation using Miniconda3-4.7.12
All packages are in /opt/miniconda/bin & are in PATH
Default runscript: blastn

Usage:
    blast_v2.9.0.sif --help
    or:
    singularity exec blast_v2.9.0.sif blastn --help


%runscript
    #default runscript: blastn passing all arguments from cli: $@
    exec /opt/miniconda/bin/blastn "$@"

%post

    #essential stuff but minimal
    apt update
    #for security fixe:
    #apt upgrade -y
    apt install -y wget bzip2

    #install conda
    cd /opt
    rm -fr miniconda

    #miniconda3: get miniconda3 version 4.7.12
    wget https://repo.continuum.io/miniconda/Miniconda3-4.7.12-Linux-x86_64.sh -O miniconda.sh

    #install conda
    bash miniconda.sh -b -p /opt/miniconda
    export PATH="/opt/miniconda/bin:$PATH"
    #add channels
    conda config --add channels defaults
    conda config --add channels bioconda
    conda config --add channels conda-forge

    #install blast
    
    conda install -y -c bioconda blast=2.9.0

    #cleanup
    conda clean -y --all
    rm -f /opt/miniconda.sh
    apt autoremove --purge
    apt clean

