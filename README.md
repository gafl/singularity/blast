# blast Singularity container
### Bionformatics package blast<br>
BLAST+ is a new suite of BLAST tools that utilizes the NCBI C++ Toolkit.<br>
blast Version: 2.9.0<br>
[http://blast.ncbi.nlm.nih.gov/Blast.cgi?PAGE_TYPE=BlastDocs]

Singularity container based on the recipe: Singularity.blast_v2.9.0

Package installation using Miniconda3-4.7.12<br>

Image singularity (V>=3.3) is automatically build and deployed (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>

### build:
`sudo singularity build blast_v2.9.0.sif Singularity.blast_v2.9.0`

### Get image help
`singularity run-help ./blast_v2.9.0.sif`

#### Default runscript: STAR
#### Usage:
  `blast_v2.9.0.sif --help`<br>
    or:<br>
  `singularity exec blast_v2.9.0.sif blastn --help`<br>


### Get image (singularity version >=3.3) with ORAS:<br>
`singularity pull blast_v2.9.0.sif oras://registry.forgemia.inra.fr/gafl/singularity/blast/blast:latest`


